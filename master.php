<?php 

$fullname = "Stranger";

if(isset($_SESSION['user']))
  $fullname = $_SESSION['user']['surname'] ." " . $_SESSION['user']['othernames'];

 ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title><?php echo $pageTitle?></title>

    <!-- Bootstrap core CSS -->
    <link href="./assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="./assets/cover.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="site-wrapper">

      <div class="site-wrapper-inner">

        <div class="cover-container">

          <div class="masthead clearfix">
            <div class="inner">
              <h3 class="masthead-brand">SOLABAY</h3>
              <ul class="nav masthead-nav">
                <li  class="<?php if ($pageName == "index.php") echo "active"; ?>"><a href="index.php">Home</a></li>

                <li class="<?php if ($pageName == "features.php") echo "active"; ?>"><a href="features.php">Features</a></li>

                <li class="<?php if ($pageName == "contact.php") echo "active"; ?>"><a href="#">Contact</a></li>

                <li class="<?php if ($pageName == "signup.php" || $pageName == "save.php") echo "active"; ?>"><a href="signup.php">Register</a></li>
                <li >Welcome <?php echo $fullname; ?></li>
              </ul>
            </div>
          </div>

 <?php echo $pageMainContent; ?>

      </div>

    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./assets/js/jquery.min.js"></script>
    <script src="./assets/js/bootstrap.min.js"></script>
    <script src="./assets/js/docs.min.js"></script>
  </body>
</html>
