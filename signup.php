<?php 
session_start();
define("ChangeMe", 1);
$pageName = basename(__FILE__);
$pageTitle = "SOLABAY::Sign Up";
ob_start();
?>
<div class="row">
<div class="container-fluid">
<div class="col-md-12">
<div class="col-md-10 col-md-offset-1">
<form action="save.php" method="post" class="form-horizontal span4 text-center">
  <h2>Sign up for nothing</h2>
  <br/><br/>
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-4 control-label">Surname</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="surname" id="surname" placeholder="Enter Surname">
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail3" class="col-sm-4 control-label">Other Names</label>
    <div class="col-sm-8">
      <input type="text" class="form-control" name="othernames" id="othernames" placeholder="Enter Other Names">
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail3" class="col-sm-4 control-label">Date of birth</label>
    <div class="col-sm-8">
      <input type="number" class="form-control" name="dob" id="dob" placeholder="Enter Date of Birth">
    </div>
  </div>

  <div class="form-group">
    <label for="inputEmail3" class="col-sm-4 control-label">Email</label>
    <div class="col-sm-8">
      <input type="email" class="form-control" name="email" id="inputEmail3" placeholder="Email">
    </div>
  </div>

  <div class="form-group">
    <label for="inputPassword3" class="col-sm-4 control-label">Password</label>
    <div class="col-sm-8">
      <input type="password" class="form-control" name="password" id="inputPassword3" placeholder="Password">
    </div>
  </div>

  <div class="form-group">
    <label for="inputPassword3" class="col-sm-4 control-label">Comfirm Password</label>
    <div class="col-sm-8">
      <input type="password" class="form-control" name="c_password" id="c_assword" placeholder="Enter Password Again">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-1 col-sm-10">
      <div class="checkbox">
        <label>
          <input type="checkbox"> Remember me
        </label>
      </div>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-1 col-sm-10">
      <button type="submit" class="btn btn-default">Register for nothing</button>
    </div>
  </div>
</form>
</div>
</div>
</div>
</div>
<?php

$pageMainContent = ob_get_contents();
ob_end_clean();
include("master.php");
?> 